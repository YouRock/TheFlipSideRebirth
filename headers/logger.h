//
// Created by kaz19 on 10.08.2017.
//
#pragma once

#include "base_inc&types.h"
#include <fstream>

enum level {
    DEBUG,
    INFO,
    WARNING,
    ERRORS,
    CRITICAL,
};

class logger {
public:
    logger(level l);
    logger(level l, c_str file_name);
    logger &operator<<(c_str s);
    logger &operator()(level l);
    logger &operator()() { return *this; }

    void setLogLevel(level l) { log_level = l; }
    level getLogLevel() { return log_level; }

private:
    c_str file_name;
    level log_level;
    level current_log_level;
};

