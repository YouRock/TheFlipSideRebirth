#pragma once
#include "base_inc&types.h"

class Editor {
public:
	void Add();
	void Edit();

private:
	void AddAction();
	void AddCard();
	void AddEvent();
	

};