#pragma once

#include "../game/Messages.h"

class ByteArray { //Instead of a char*
public:
	ByteArray(char* c) : data(c) {};
	typeOfMessage getNextType(); //look on next byte
	MessageChangePlayerScore getMessageChangePlayerScore(); //get this message

	~ByteArray() {
		delete data;
	}


private:
	int i = 0;
	char* data;
};