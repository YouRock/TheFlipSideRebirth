//
// Created by kaz19 on 07.08.2017.
//

#pragma once

#include "base_inc&types.h"
#include <exception>
#include <sstream>
#include <utility>

class NetException : public std::exception{
public:
    explicit NetException(std::string message) : message(std::move(message)) {}
    explicit NetException(int i) : error_code(i){}
    NetException(std::string message, int i) : message(std::move(message)), error_code(i){}
    std::string getMessage(){
        std::stringstream ss;
        ss << error_code;
        return message+" "+ss.str();}

private:
    int error_code{};
    std::string message;
};


