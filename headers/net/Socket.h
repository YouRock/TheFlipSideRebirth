//
// Created by kaz19 on 14.07.2017.
//

#pragma once

#define PLATFORM_WINDOWS  1
#define PLATFORM_MAC      2
#define PLATFORM_UNIX     3

#if defined(_WIN32)
    #define PLATFORM PLATFORM_WINDOWS
#elif defined(__APPLE__)
    #define PLATFORM PLATFORM_MAC
#else
    #define PLATFORM PLATFORM_UNIX
#endif

#if PLATFORM == PLATFORM_WINDOWS
    #include <winsock2.h>
#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <fcntl.h>
#endif

#if PLATFORM == PLATFORM_WINDOWS
    #pragma comment( lib, "wsock32.lib" )
#endif

#include "base_inc&types.h"
#include "NetException.h"
#include <ws2tcpip.h>


using namespace std;
/**
 * Класс для работы с socket под любую платформу
 * @autor Казанцев Андрей
 * @version 1.0
*/

class Socket {
public:
    explicit Socket();
    explicit Socket(SOCKET s);
    explicit Socket(string port);
    Socket(string ip, string port);

    Socket* setBlocking(bool b = true);

    Socket* max_listen(int n);
    Socket* bind(string ip, string port);
    Socket* connect(string ip, string port);

    Socket* accept();

    int send(string s);
    int send(string s, int len);
    int send(bytes b, size_t len);
    //int sendto(string ip, string port, char* s, size_t len);

    char* read(int recvbuflen);

    int close();

private:
    int initHint();
    int initSocket();

    SOCKET id_socket;
    addrinfo hints;

};