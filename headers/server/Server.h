//
// Created by kaz19 on 13.07.2017.
//
#pragma once

#include "net/Socket.h"
#include <windows.h>
#include <thread>

typedef void (*func_ptr)(Socket*);

class Server {
public:
    Server(string port);
    Server(string port, func_ptr f);
    Server(string port, func_ptr f, int n);
    int setBufferSize(int n);
    int setProcessFunc(func_ptr f);
    int startServer();
    int closeServer();
    thread* getListenThread();
    //void handle();

private:
    int startWaitConnect();
    int stopWaitConnect();

    int bufferSize = 512;
    int maxListen = 1000;
    func_ptr processFunc;
    string port;
    Socket *listen_sock;
    thread *waitConnected;
};

