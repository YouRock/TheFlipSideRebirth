#pragma once
#include "net/Socket.h"

class Client
{
    public:
        Client(c_str Address, c_str Port);
        void setPort(c_str Port);
        void setAddress(c_str Address);
        Socket* getSocket();
        void Close();

    private:
        c_str Port;
        c_str Address;
        Socket ClientSocket;
};