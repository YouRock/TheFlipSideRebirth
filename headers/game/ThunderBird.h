#pragma once

#include "Event.h"

class ThunderBird : public Event {
public:
	ThunderBird() {};
	void TurnOn(){}
	void Affect(vector<Player*>&);
};