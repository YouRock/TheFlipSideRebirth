#pragma once

#include "Event.h"

class Source : public Event {
public:
	Source(int p) : period(p) {
		Event();
	}

	virtual void Affect(vector<Player*>&);
	virtual void TurnOn();

private:
	int period;
};