#pragma once

#include "base_inc&types.h"
#include "net/Socket.h"
#include "tools/json.h"

/*!!!README!!!
If you want to add new type of message, then you need to:
	1) Create new member in enum typeOfMessage (this can be a action, headline of a message ...).
	2) Create new struct MyMessage : public Message {...variables that you need...};
	3) From Messenger.cpp in CheckMessage() in switch create new case MY_TYPE_OF_MESSAGE - typeOfMessage that you created in step 1, 
		and write there actions that receiver must do, when it get your message*/

enum class typeOfMessage { START_MES, CHANGE_PLAYER_SCORE, SHOW_NEXT_EVENT, SHOW_NEXT_CARD, USE_CARD, USE_EVENT ,JSON_MES}; //send ahead of the message object

/*What type corresponds to which message

CHANGE_PLAYER_SCORE -> MessageChangePlayerScore
SHOW_NEXT_EVENT, SHOW_NEXT_EVENT -> MessageWithIndexNextAction
USE_CARD, USE_EVENT -> NullMessage*/


struct Message { //Базовая структура в которой будет лежать тип сообщения
    typeOfMessage type; // В конструкторе наследника указываем какой тип
};


class jMessage : public Message{ // На всякий случай
public:
    explicit jMessage();
    explicit jMessage(string s);
    jMessage& operator[](string i);
    jMessage& operator=(string in);
    //jMessage& getMessage(Socket*);
    explicit operator string ();
    explicit operator c_str ();
    void send_to(Socket*);

private:
    map<string,string> mes;
    string point;
};

class Player;
struct StartMessage : public Message {
	int* CardsOnTable;
	int* CardsInPlayerHands;
	Player* players;

	StartMessage(){
		type = typeOfMessage::START_MES;
    }
};

struct MessageChangePlayerScore : public Message {
	int player, deltaScore;
    MessageChangePlayerScore(){
        //type =
    }
};

struct MessageWithIndexNextAction {
	int index;
};
