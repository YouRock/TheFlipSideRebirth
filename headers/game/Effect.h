#pragma once

#include "Player.h"
//class Player

class BaseEffect {
	int deltaScore;

public:
	BaseEffect(int ds) : deltaScore(ds) {};

	virtual void Affect(Player&);
};

class Effect : public BaseEffect {
public:
	Effect(int ds);

	virtual void Affect(Player&);
};