#pragma once

#include "Card.h"

class Act : public Card {
public:
    virtual void Affect(vector<Player>&);
};
