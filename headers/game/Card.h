#pragma once

#include "Effect.h"
#include <vector>

using namespace std;

class Player;

class Card {
	int timeOfUseAction = 0;

protected:
	char* pic = (char *) "oo"; //is it must be in client, but not exist in Game?
    vector<Effect> effects;

	void UseEffects(vector<Player*>&) const;
	void UseEffects(vector<Effect*>& eff, vector<Player*>&) const;
    virtual void Affect(Player&);

public:
	int GetTimeOfUse() const { return timeOfUseAction; }

	bool operator <(const Card right) const {
		return ( (*this).GetTimeOfUse() < right.GetTimeOfUse());
	}

	bool operator() (const Card left, const Card right) const {
		return (left.GetTimeOfUse() < right.GetTimeOfUse());
	}

	Card() = default;
	//Card(GameObjects* g) : gameObjects(g) {}
	Card(vector<Effect>& ef) : effects(ef) {};
};