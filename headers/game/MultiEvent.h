#pragma once

#include "Event.h"
#include "Meeting.h"
#include "Source.h"
#include "ThunderBird.h"

/*class MultiEvent : public Event {
	ThunderBird* tb=NULL; Meeting* m=NULL; Source* s=NULL;

public:
	MultiEvent(const ThunderBird* T, const Meeting* M, const Source* S);

	void TurnOn();
	void Affect(Player*);
	};*/

class MeetingAndThunderBird : public Event { //ready 06.07.2017
	Meeting m;
public:
	virtual void Affect(vector<Player*>&);
	virtual void TurnOn() {}
};

class ThunderBirdAndSource : public Event { //ready 06.07.2017
	ThunderBird tb; Source s;
public:
	virtual void TurnOn() { s.TurnOn(); }
	virtual void Affect(vector<Player*>& p) { tb.Affect(p); }
};

class SourceAndMeeting : public Event { //ready 06.07.2017
	Source s; Meeting m;
public:
	virtual void TurnOn() { s.TurnOn(); }
	virtual void Affect(vector<Player*>& p) { m.Affect(p); }
};

class ThunderBirdAndMeetingAndSource { //ready 06.07.2017
	SourceAndMeeting sm;

public:
	virtual void TurnOn() { sm.TurnOn();}
	virtual void Affect(vector<Player*>& p) { sm.Affect(p); }
};