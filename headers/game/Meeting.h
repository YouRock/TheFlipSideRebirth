#pragma once

#include "Event.h"

class Meeting : public Event {
	const int countOfAltern;
	vector<Player*>players;
	vector<vector<Effect*> > alternatives; //����������, ��-�� ���� ����� ������� ������
	vector<int> votes;
	int countOfVotes=0;

public:
	void Affect(vector<Player*>&);
	bool ThereIsTradeOff(int &ind);
	void AddVote(int numberAlt);
	void TurnOn() {}

	Meeting(vector<Player*>& p, int count, vector<vector<Effect*> > efs) :
		countOfAltern(count),
		players(p),
		alternatives(efs),
		votes(countOfAltern, 0)
		
	{};
};
