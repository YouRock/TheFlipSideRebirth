#pragma once

#include "base_inc&types.h"
#include "game/User.h"

class Card;

class Player : public User {
	int score = 500, recMana=0;
	char colour;

	std::vector<Card*> handOfCards;

public:
	int i(){ return 10;}
	Player(std::vector<Card*> hc) : handOfCards(hc){};

	void SetMana(int m);
	void SetScore(int s);
	void SetColour(char c);
	void ChangeScore(int delta);
	void ChangeMana(int delta);
	int GetMana();
	int GetScore();
	int GetColour();
};