#pragma once

#include "Effect.h"

class OneTimeEffect : public BaseEffect {
public:
	OneTimeEffect(int ds);

private:
	void Affect();
};
