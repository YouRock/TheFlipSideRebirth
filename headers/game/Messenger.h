#pragma once

#include <queue>
#include <vector>

#include "Messages.h"
#include "../../headers/game/GameObjects.h"
#include "../net/ByteArray.h"
#include "../net/Socket.h"

/*!!!README!!!
	If you want to send message, then you need to use (Client or Server)Messenger.sendMessage(...)
	If yout want to put message, then you need to use (Client or Server)Messenger.pushMessage(...)*/

class Messenger {
public:
    virtual ~Messenger() = 0;
    void NotifyOfChangePlayerScore(int player, int deltaScore);
    //void NotifyShowNextEvent() { sendMessage(SHOW_NEXT_EVENT, NullMessage()); }
    //void NotifyShowNextCard() { sendMessage(SHOW_NEXT_CARD, NullMessage()); }

    void CheckMessages(GameObjects& gameObj);
    void PushMessage(char* s) { inputMessages.push(ByteArray(s));} //be carefull, char* must contains ONE MESSAGE! //15.08.2017

	template <typename MessageType>
    int sendMessage(Socket*, MessageType);
    Message* getNextMessage(Socket*);

private:

	std::queue<ByteArray> inputMessages; //Socket put char* here
};

class ClientMessenger : public Messenger {
public:
    void InitializeSocket(Socket* s) { socket = s; };
    virtual ~ClientMessenger() {
        delete socket;
    }

	template <typename MessageType>
	int sendMes(typeOfMessage type, MessageType mes) { return sendMessage(socket, mes); };
private:
    Socket* socket;
};

class ServerMessenger : public Messenger {
public:
    void AddSocket(Socket* s) { sockets.push_back(s); }
    virtual ~ServerMessenger() {};

	template <typename MessageType>
	int sendMessageAllUsers(typeOfMessage type, MessageType mes);


private:
    std::vector<Socket*> sockets;
};

/*struct Message {
	Messager::comand cmd;
	stack<int> operators;

	Message() {};
	Message(Messager::comand c) : cmd(c) {};
};*/



