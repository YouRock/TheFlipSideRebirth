﻿#pragma once

#include "Config.h"
#include "GameObjects.h"
#include "Messenger.h"

#include <time.h>
#include <vector>


using namespace std;

class Game {
    GameObjects gameObjects; //must be initializated in constructor
	const int startOfGame;
	bool win = false;
	Config config;

	ServerMessenger messenger;

    Game();

	void StartGame();
	void InitializeClients();
	void WaitResponseOfAllClients();

	void Play();

	bool EndOfGame();
	void CheckMessages();


public:

};