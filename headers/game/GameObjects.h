﻿#pragma once

#include "Card.h"
#include "Event.h"
#include "Player.h"

#include <set>
#include <queue>
#include <ctime>

using namespace std;

class GameObjects {
public:	
	void AddHappening(Card);	
	int GetNumberOfPlayers() { return players.size(); }	
	void CarryOutAllActions();

	void ChangePlayerScore(int player, int delta);
private:	
	vector<Player>players;	
	set<Card> happenings;	
	queue<Event> events;	

	bool SomethingHappen() { return happenings.begin()->GetTimeOfUse() >= time(NULL); } //проверяет нет ли чего, что должно было случится по плану#pragma once
};