﻿#pragma once

class Config {
    int numOfPlayers, timeOfGame, maxMana;
public:

    void Init() {
      numOfPlayers = 4;
      timeOfGame = 2 * 60 * 1000;
      maxMana = 5 * 1000;
    }

    int GetNumOfPlayers() {
      return numOfPlayers;
    }

    int GetTimeOfGame() {
      return timeOfGame;
    }

    int GetMaxMana() {
      return maxMana;
    }
};