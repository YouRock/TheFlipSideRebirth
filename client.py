from socket import socket
from threading import Thread
from time import sleep

sock = socket()
sock.connect(('localhost', 1000))

def read(sock):
    while 1:
        data = sock.recv(1024)
        print(data)

def write(sock):
    while 1:
        s = input()
        sock.send(s.encode())

Thread(target=read,args=(sock,),daemon=True).start()
Thread(target=write,args=(sock,),daemon=True).start()
sleep(1000)