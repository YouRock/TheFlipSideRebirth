#include "client/Client.h"
#include <gtest/gtest.h>
#include "server/Server.h"

void f(Socket*);

TEST(Net,CientConnect){
    Server ser("1000",f);
    ser.startServer();
    Client cl("127.0.0.1","1000");
    Socket* sock = cl.getSocket();
    sock->send("hi");
    EXPECT_EQ(string(sock->read(1024)),string("{\"help\": \"Hello\"}"));
    ser.closeServer();
}
