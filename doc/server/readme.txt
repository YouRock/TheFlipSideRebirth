Класс сервер имеет три конструктора. Один принимает порт, второй ещё и функцию void f(Socket) - обработчик которая
запускается при подключении клиента в отдельном потоке, а третий ещё и максимальное колличество клиентов в очереди
на обработку. Для запуска сервера надо вызвать метод startServer, а для остановки stopServer.