#include <list>
#include <utility>
#include "server/Server.h"

Server::Server(string port){
    this->port = port.empty() ? "1024" : port;
    listen_sock = new Socket(std::move(port));
}

Server::Server(string port, func_ptr f) {
    this->port = port.empty() ? "1024" : port;
    listen_sock = new Socket(std::move(port));
    this->processFunc = f;
}

Server::Server(string port, func_ptr f, int n) {
    this->port = port.empty() ? "1024" : port;
    listen_sock = new Socket(std::move(port));
    this->processFunc = f;
    this->maxListen = n;
}

int Server::startServer() {
    waitConnected = new thread(&startWaitConnect,this);
    waitConnected->detach();
    return 0;
}


int Server::startWaitConnect() {
    listen_sock->max_listen(maxListen);
    Socket* ClientSocket;
    try{
        for (;;) {
            ClientSocket = listen_sock->accept();
            (new thread(processFunc,ClientSocket))->detach();
        }
    }catch (NetException i){
        cout << i.getMessage() << endl;
    }
    return 0;
}

int Server::closeServer() {
    stopWaitConnect();
    return 0;
}

thread* Server::getListenThread() {
    return waitConnected;
}

int Server::stopWaitConnect() {
    listen_sock->close();
    return 0;
}

int Server::setBufferSize(int n) {
    this->bufferSize = n;
    return 0;
}

int Server::setProcessFunc(func_ptr f) {
    this->processFunc = f;
    return 0;
}
