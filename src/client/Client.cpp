#include "client/Client.h"

void Client :: setPort(c_str PortToSet)
{
    Port = PortToSet;
}

void Client :: setAddress(c_str AddressToSet)
{
    Address = AddressToSet;
}

Client :: Client(c_str Address, c_str Port)
{
    setPort(Port);
    setAddress(Address);
    ClientSocket.connect(Address, Port);
}

Socket * Client::getSocket()
{
    return &ClientSocket;
}

void Client :: Close()
{
    ClientSocket.close();
}

void StartClient()
{
    try {
        Socket* connect = new Socket("127.0.0.1","1000");
        connect->send("hi");
        connect->read(512);
        connect->close();
    }catch (int i){
        cout << i << endl;
    }
}