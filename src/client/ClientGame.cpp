#include <iostream>
#include <thread>
#include "net/Socket.h"
#include "client/Client.h"
#include "client/ClientSettings.h"
#include "game/Messenger.h"

using namespace std;

void SendMessages(Socket * ConnectedToMainServer, int MessageBufferSize);
void ReceiveMessages(Socket * ConnectedToMainServer, Socket * ConnectedToLocalServer, int MessageBufferSize);
void StartClient();

int main(){
    StartClient();
    return 0;
}

void SendMessages(Socket * ConnectedToMainServer, int MessageBufferSize)
{
    char * MessageBuffer = new char[DefaultMessageBufferSize];
    do
    {
        cout << "Type the message you want to send :" <<endl;
        cin.getline(MessageBuffer, MessageBufferSize);
        ConnectedToMainServer -> send(MessageBuffer);
    }
    while(strcmp(MessageBuffer, "CloseChat") != 0);
    delete MessageBuffer;
}

void ReceiveMessages(Socket * ConnectedToMainServer, Socket * ConnectedToLocalServer, int MessageBufferSize)
{

    char * ReceivedMessage = new char[MessageBufferSize];
    do
    {
        ReceivedMessage = ConnectedToMainServer -> read(MessageBufferSize);
        ConnectedToLocalServer -> send(ReceivedMessage);
    }
    while(strcmp(ReceivedMessage, "CloseChat") != 0);
    delete ReceivedMessage;
}