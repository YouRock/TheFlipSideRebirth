#include "net/Socket.h"

static int iResult;

inline void InitializeSocket()
{
#if PLATFORM == PLATFORM_WINDOWS
    WSADATA wsaData;
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        throw NetException("WSAStartup failed with error: ", iResult);
    }
#endif
}

inline void ShutdownSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
    WSACleanup();
#endif
}

static addrinfo* makeAddrInfo(const string& ip, const string& port, addrinfo& hints) {
    /**
     * Локальная функция строящая addrinfo на основе hints(шаблона)
     */
    addrinfo *result = NULL;

    iResult = getaddrinfo(ip.c_str(), port.c_str(), &hints, &result);
    if ( iResult != 0 ) { ShutdownSockets();
        throw NetException("getaddrinfo failed with error: ", iResult);
    }
    return result;
}

Socket::Socket() {
    InitializeSocket();
    initHint();
    initSocket();
}

Socket::Socket(SOCKET s) {
    InitializeSocket();
    initHint();
    id_socket = s;
}

Socket::Socket(string port) {
    InitializeSocket();
    initHint();
    initSocket();
    bind("0.0.0.0",port);
}

Socket::Socket(string ip, string port){
    InitializeSocket();
    initHint();
    initSocket();
    connect(ip,port);
}


Socket* Socket::setBlocking(bool b){
    u_long i = b ? 0:1;
    iResult = ioctlsocket(id_socket, FIONBIO, &i);
    if ( iResult < 0)
        printf("Set exception failed");
    return this;
}


int Socket::initHint() {
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;
    return 0;
}

int Socket::initSocket() {
    id_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (id_socket == INVALID_SOCKET) {
        iResult = WSAGetLastError(); ShutdownSockets();
        throw NetException("socket failed with error: ", iResult);
    }
    return 0;
}

Socket* Socket::bind(string ip, string port) {
    addrinfo* result = makeAddrInfo(ip,port,hints);
    iResult = ::bind(id_socket, result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        iResult = WSAGetLastError();
        freeaddrinfo(result);
        closesocket(id_socket);
        ShutdownSockets();
        throw NetException("bind failed with error: ", iResult);
    }
    return 0;
}

Socket* Socket::max_listen(int n) {
    iResult = listen(id_socket, n);
    if (iResult == SOCKET_ERROR) {
        iResult = WSAGetLastError();
        closesocket(id_socket);
        ShutdownSockets();
        throw NetException("listen failed with error: ", iResult);
    }
    return this;
}

Socket* Socket::accept() {
    SOCKET ClientSocket;
    sockaddr *ret = new sockaddr;
    int size = sizeof(sockaddr);
    ZeroMemory(&ret, (size_t) size);

    ClientSocket = ::accept(id_socket, ret, &size);
    if (ClientSocket == INVALID_SOCKET) {
        iResult = WSAGetLastError();
        closesocket(id_socket);
        ShutdownSockets();
        throw NetException("accept failed with error: ", iResult);
    }
    return new Socket(ClientSocket);
}

char* Socket::read(int recvbuflen) {
    char *recvbuf = new char[recvbuflen];
    iResult = recv(id_socket, recvbuf, recvbuflen, 0);
    if (iResult > 0) {
        recvbuf[iResult]='\0';
        return recvbuf;
    }
    iResult = WSAGetLastError();
    if (iResult == 0 or iResult == 10035) return NULL;
    else throw NetException("Error load: ",iResult);
}

int Socket::send(string s){
    send(s, s.length() );
    return 0;
}

int Socket::send(bytes s, size_t len) {
    send(s, len );
}

int Socket::send(string s, int len){
    int iSendResult = ::send(id_socket, s.c_str(), len, 0);
    if (iSendResult == SOCKET_ERROR) {
        iResult = WSAGetLastError();
        closesocket(id_socket);
        ShutdownSockets();
        throw NetException("send failed with error: ", iResult);
    }
    return 0;
}

int Socket::close(){
    closesocket(id_socket);
    return 0;
}

Socket* Socket::connect(string ip, string port) {
    addrinfo* result = makeAddrInfo(ip,port,hints);
    iResult = ::connect(id_socket,result->ai_addr, (int)result->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        iResult = WSAGetLastError();
        closesocket(id_socket);
        throw NetException("connect failed with error: ", iResult);
    }
    return this;
}
