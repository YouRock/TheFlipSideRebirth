#include "../../headers/net/ByteArray.h"

typeOfMessage ByteArray::getNextType() {
	typeOfMessage t = typeOfMessage(data[i]);
	i++;

	return t;
}

MessageChangePlayerScore ByteArray::getMessageChangePlayerScore() {
	MessageChangePlayerScore m = *(reinterpret_cast<MessageChangePlayerScore*>(data + i));
	i += sizeof(MessageChangePlayerScore);
	return m;
}