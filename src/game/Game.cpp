#include "../../headers/game/Game.h"
#include <time.h>
#include <iostream>

Game::Game() : startOfGame(time(NULL)) {
	//must be more actions with gameobj
};


void Game::StartGame() {
	InitializeClients();
	WaitResponseOfAllClients();

	Play();

	//something
}

void Game::InitializeClients() {

}

void Game::WaitResponseOfAllClients() {

}

void Game::Play() {
	const int delayValue = 10;

	while (!EndOfGame()) { //gameloop
		CheckMessages();

		gameObjects.CarryOutAllActions(); //check events that already occurred

		//ZaRus!!!
		//Update(); 

		_sleep(delayValue);
	}
}

void Game::CheckMessages() {
	//it is already done in Messenger. Maybe delete CheckMessages()?
}

bool Game::EndOfGame() {
	return win || time(0) >= startOfGame + config.GetTimeOfGame();
}



