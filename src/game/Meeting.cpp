#include "../../headers/game/Meeting.h"

void Meeting::Affect(vector<Player*>& p) {
	int indexOfAlt; //��� ������������ - ThereIsTradeOff ���������� �� ������

	if (ThereIsTradeOff(indexOfAlt))
		Card::UseEffects(alternatives[indexOfAlt], p);
	else
		Card::UseEffects(p); //���������� � Card �������� ������� ���� ������, ����� ������ �� ������ � �����������
}

bool Meeting::ThereIsTradeOff(int &ind) {
	int mxVotes = 0;
	bool thereIsTradeOff = true;

	for (size_t i = 0; i < countOfAltern; i++)
		if (mxVotes < votes[i]) {
			mxVotes = votes[i];
			ind = i;
		}

	for (size_t i = 0; i < countOfAltern; i++)
		if (mxVotes == votes[i] && ind != i)
			thereIsTradeOff = false;

	return thereIsTradeOff;
}

void Meeting::AddVote(int numberAlt) { 
	votes[numberAlt - 1]++;
	countOfVotes++; 

	if (countOfVotes == players.size()) Affect(players);
}