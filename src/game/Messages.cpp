//
// Created by kaz19 on 14.08.2017.
//

#include <utility>

#include "game/Messages.h"

jMessage::jMessage(){
    type = typeOfMessage::JSON_MES;
}

jMessage::jMessage(string s){
    type = typeOfMessage::JSON_MES;
    string err;
    json::Json j = json::Json::parse(s,err);

}

jMessage& jMessage::operator[](std::string i){
    point =std::move(i);
    return *this;
}

jMessage& jMessage::operator=(std::string in){
    mes[point] =std::move(in);
    return *this;
}

void jMessage::send_to(Socket* sock) {
    json::Json send_mes(mes);
    sock->send(send_mes.dump());
}

jMessage::operator string (){
    return mes[point];
}

jMessage::operator c_str () {
    return mes[point].c_str();
}