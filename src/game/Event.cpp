#include "../../headers/game/Event.h"
#include <ctime>

void Event::TurnOn() { 
	timeOfStartEvent = time(NULL);
}

bool Event::TimeIsOver() {
	return timeOfEndEvent >= time(NULL);
}
