//
// Created by kaz19 on 10.08.2017.
//
#include "logger.h"

logger::logger(level l) {
    log_level = l;
    this->file_name = "log.txt";
}

logger::logger(level l, c_str file_name) {
    log_level = l;
    this->file_name = file_name;
}

logger& logger::operator<<(c_str s){
    if (current_log_level>=log_level){
        ofstream file(file_name, ios_base::app);
        file << s;
        file.close();
    }
    return *this;
}

logger& logger::operator()(level l){
    current_log_level = l;
    return *this;
}

